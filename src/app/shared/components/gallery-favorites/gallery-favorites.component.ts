import { Component, Input, OnInit } from '@angular/core';
import { FavoritesLocalService } from '../../services/local/favorites-local.service';

@Component({
  selector: 'app-gallery-favorites',
  templateUrl: './gallery-favorites.component.html',
  styleUrls: ['./gallery-favorites.component.scss']
})
export class GalleryFavoritesComponent implements OnInit {

  constructor(private favoritesService:FavoritesLocalService) { }

  @Input() items:any[]= []
  ngOnInit(): void {
  }



  removeFav(newFavorite:any){
    this.favoritesService.removeFavorites(newFavorite)
  }
}
